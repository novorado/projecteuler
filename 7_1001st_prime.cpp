#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

//#define Nth 6
#define Nth 10001

bool is_prime(num_t n) {
  if (n == 1)
    return false;
  if (n < 4)
    return true;
  if (n % 2 == 0)
    return false;
  if (n < 9)
    return true;
  if (n % 3 == 0)
    return false;
  /*
  Any number n can have only one primefactor greater than n .
  The consequence for primality testing of a number n is: if we cannot find a
  number f less than or equal n that divides n then n is prime: the only
  primefactor of n is n itself
  All primes greater than 3 can be written in the form 6k+/-1.
  */
  num_t r = floor(sqrt(n));
  num_t f = 5;
  while (f <= r) {
    if (n % f == 0)
      return false;
    if (n % (f + 2) == 0)
      return false;
    f += 6;
  }
  return true;
}

int main(int, char **) {
  size_t N = 1;
  num_t i = 2;
  for (; N <= Nth;) {
    if (is_prime(i)) {
      cout << N++ << "= " << i << endl;
    }
    i++;
  }
  return 0;
}
