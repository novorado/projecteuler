#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = long;

struct coord {
  num_t x{0}, y{0};
  void operator+=(const coord &ano) {
    x += ano.x;
    y += ano.y;
  }
  bool operator!=(const coord& ano) 
  {
      return x != ano.x || y != ano.y;
  }
};

//#define T 5
#define T 1001

struct Matrix {
  array<int, T * T> data;
  Matrix() { fill(data.begin(), data.end(), 0); }

  int &operator()(const coord &c) { return data[c.x * T + c.y]; }
  bool isFree(const coord &c) { return (*this)(c) == 0; }
  string str() {
    stringstream ss;
    for (int y = T - 1; y >= 0; y--) {
      for (int x = 0; x < T; x++) {
        ss << setw(3) << (*this)({x, y});
      }
      ss << "\n";
    }
    return ss.str();
  }
};

enum class Dir { right, down, left, up };

void next(coord &c, Dir dir) {
  switch (dir) {
  case Dir::right:
    c.x += 1;
    break;
  case Dir::down:
    c.y -= 1;
    break;
  case Dir::left:
    c.x -= 1;
    break;
  case Dir::up:
    c.y += 1;
    break;
  }
}

void tryTurningClockwise(coord c, Dir &dir, Matrix &m) {
  switch (dir) {
  case Dir::right:
    if (m.isFree({c.x, c.y - 1}))
      dir = Dir::down;
    break;
  case Dir::down:
    if (m.isFree({c.x - 1, c.y}))
      dir = Dir::left;
    break;
  case Dir::left:
    if (m.isFree({c.x, c.y + 1}))
      dir = Dir::up;
    break;
  case Dir::up:
    if (m.isFree({c.x + 1, c.y}))
      dir = Dir::right;
    break;
  }
}

int sumOfDiagonals(Matrix &m) {
  coord walker1{0, 0}, walker2{0, T - 1};
  int rv{0};
  for (int i = 0; i < T; i++) {
    auto v1 = m(walker1);
    auto v2 = m(walker2);
    walker1 += {1, 1};
    walker2 += {1, -1};
    rv += v1 + v2;
  }
  return rv - 1 /* center is counted twice */;
}

int main(int, char **) {
  try {
    Matrix m;
    num_t c = (T - 1) / 2;
    coord walker{c, c}; // always wants to turn right when it's free
    // Fill
    size_t val{1};
    Dir dir{Dir::up};
    for (;;) {
      if (!m.isFree(walker)) {
        stringstream ss;
        ss << "Cell " << walker.x << "," << walker.y << " used by " << m(walker)
           << endl;
        throw logic_error(ss.str());
      }
      m(walker) = val++;
      // cout << m.str() << endl;
      if (val > T * T) {
        // most likely we're good
        break;
      }
      tryTurningClockwise(walker, dir, m);
      next(walker, dir);
    }
    cout << "M[" << T << "x" << T
         << "] sum of diagonals = " << sumOfDiagonals(m) << endl;

  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
