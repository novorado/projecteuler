#include <algorithm>
#include <cmath>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;
using num_t = long;
#define N 3

string complete(string S) {
  while (S.length() < N) {
    S = '0' + S;
  }
  return S;
}

string inversed(string S) {
  string rv = S;
  for (num_t i = 0; i < N; i++) {
    rv[i] = S[N - i - 1];
  }
  return rv;
}

bool is_reversed(num_t R, num_t L) {
  stringstream rss, lss;
  rss << R;
  lss << L;
  string Rs = complete(rss.str()), Ls = complete(lss.str());
  return Rs == inversed(Ls);
}

int main(int, char **) {
  const num_t pow_n = pow(10, N);
  const num_t start = pow_n - 1;
  num_t maxPal = 1;
  for (num_t i = start; i >= 1; i--) {
    for (num_t j = start; j >= 1; j--) {
      num_t mul = i * j;
      num_t R = mul / pow_n, L = (mul - R * pow_n);
      if (is_reversed(R, L)) {
        if (maxPal < mul) {
          maxPal = mul;
          cout << "i=" << i << " j=" << j << " maxPal=" << maxPal << endl;
        }
      }
    }
  }
  return 0;
}
