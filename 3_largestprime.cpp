#include <iostream>
using namespace std;
int main(int, char **) {
  using num_t = long;
  num_t num = 600851475143;
  // num = 13195; // test
  num_t maxPrime = 1;
  for (num_t i = 2; i <= num; i++) {
    if (num % i == 0) {
      maxPrime = max(maxPrime, i);
      num /= i;
      // cout << "Prime = " << i << endl;
    }
  }
  cout << "Max prime is " << maxPrime << endl;
  return 0;
}
