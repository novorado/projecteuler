#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

using num_t = unsigned long;
int main(int, char **) {
  const num_t upperBound = 500;
  for (num_t a = 1; a <= upperBound; a++) {
    for (num_t b = 1; b <= upperBound; b++) {
      auto qsum = a * a + b * b;
      num_t c = sqrt(qsum);
      if (c * c == qsum && a + b + c == 1000) {
        cout << "a=" << a << " b=" << b << " c=" << c
             << " a+b+c=" << (a + b + c) << endl;
        cout << "a^2=" << (a * a) << endl;
        cout << "b^2=" << (b * b) << endl;
        cout << "c^2=" << (c * c) << endl;
        cout << "a*b*c= " << (a*b*c) << endl;
        return 0;
      }
    }
  }
  return 0;
}
