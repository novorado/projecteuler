#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

#define T 1000000

num_t next(num_t v) {
  if (v % 2) {
    return 3 * v + 1;
  }
  return v / 2;
}

vector<num_t> cash(T);

size_t length(num_t v) {
    auto v1=v;
  size_t rv{1};
  for (;;) {
    v = next(v);
    if (v>2 && v<T && cash[v]) {
      rv += cash[v];
      cash[v1] = rv;
      return rv;
    }
    rv++;
    if (v == 1) {
      break;
    }
  }
  cash[v1] = rv;
  return rv;
}

int main(int, char **) {
  fill(cash.begin(), cash.end(), 0x0);
  pair<num_t, num_t> maxP{1, 1};
  try {
    for (num_t i = 2; i < T; i++) {
      size_t lg = length(i);
      if (maxP.second < lg) {
        maxP = {i, lg};
      }
    }
    cout << "Max N=" << maxP.first << " length=" << maxP.second << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
