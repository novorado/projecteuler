#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
#include <iomanip>
using namespace std;

int connectedSum(int n, vector<string> edges) {
  vector<set<int>> groups;
  for (auto &input : edges) {
    const char *s1{NULL}, *s2{NULL};
    s1 = &input[0];
    int n1{0}, n2{0};
    for (char &ch : input) {
      if (ch == ' ') {
        ch = 0x0;
        n1 = stoi(s1);
        s2 = &ch;
        s2++;
        n2 = stoi(s2);
        break;
      }
    }
    if (!s1 || !s2) {
      continue;
    }
    bool flagJoined{false};
    for (auto &group : groups) {
      if (group.find(n1) != group.end()) {
        flagJoined = true;
      }
      if (group.find(n2) != group.end()) {
        flagJoined = true;
      }
      if (flagJoined) {
        group.insert(n1);
        group.insert(n2);
      }
    }
    if (!flagJoined) {
      groups.emplace_back();
      groups.back().insert(n1);
      groups.back().insert(n2);
    }
    n -= 2;
  }
  int rv = 0;
  for (auto &g : groups) {
    rv += std::ceil(sqrt(g.size()));
  }
  return rv + n;
}

int main(int, char **) {
  try {
   int rv=connectedSum(4,{"1 2","1 4"});
   cout << rv << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
