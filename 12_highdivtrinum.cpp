#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

bool is_prime(num_t n) {
  if (n == 1)
    return false;
  if (n < 4)
    return true;
  if (n % 2 == 0)
    return false;
  if (n < 9)
    return true;
  if (n % 3 == 0)
    return false;
  /*
  Any number n can have only one primefactor greater than n .
  The consequence for primality testing of a number n is: if we cannot find a
  number f less than or equal n that divides n then n is prime: the only
  primefactor of n is n itself
  All primes greater than 3 can be written in the form 6k+/-1.
  */
  num_t r = floor(sqrt(n));
  num_t f = 5;
  while (f <= r) {
    if (n % f == 0)
      return false;
    if (n % (f + 2) == 0)
      return false;
    f += 6;
  }
  return true;
}

void generate_primes(vector<num_t> &primes, size_t sz) {
  primes.reserve(sz);
  for (num_t p = 2; primes.size() < sz; p++)
    if (is_prime(p))
      primes.push_back(p);
}

//#define Ndivisors 500
#define Ndivisors 10
#define PRIMES_SIZE 65500

int main(int, char **) {
  try {
    vector<num_t> primes;
    generate_primes(primes, PRIMES_SIZE);
    //
    num_t n = 3;   // start with a prime
    num_t Dn = 2;  // initial number of divisors - 1 and number itself
    num_t cnt = 0; // to insure the while loop is entered
    num_t n1{0}, Dn1{0}, exponent{0};
    while (cnt <= Ndivisors) {
      // Triangle number formula Tn=n*(n+1)/2
      // Any integer is factorized like
      //  N = p1^a1 * p2^a2 * p3^a3 * ... where Pi is a prime number
      // So number of divisors can be calculated like
      //  (a1+1)*(a2+1)*(a3+1)*..
      // For example, T7=2^2*7^1,
      //  number of divisors D(T7=28)=(2+1)*(1+1)=3*2=6
      //  indeed, Triangle number 7 T7=28 has 6 divisors 1,2,4,7,14,28
      //        1] (2*2*7)=28
      //        2] (2)=2
      //        3] (7)=7
      //        4] (2*7)=14
      //        5] (2*2)=4
      //        6] 1
      // Tn=n*(n+1)/2
      // D(Tn)=D( n*(n+1)/2 )
      //  even n: D( Tn ) = D(n/2) * D(n+1)
      //  even n+1: D( Tn ) = D(n) * D ( (n+1) / 2)
      n = n + 1;
      n1 = n;
      if (n1 % 2 == 0) {
        n1 = n1 / 2;
      }
      Dn1=1;
      for (num_t i = 0; i < PRIMES_SIZE; i++) {
        auto CurrentPrime = primes[i];
        // Halt if the divisor exceeds sqrt(triangle number)
        // halting divisions when the divisor exceeds the square root of the 
        // triangle number.For every exact divisor up to the square root,
        // there is a corresponding divisor above the square root
        if (CurrentPrime * CurrentPrime > n1) {
          Dn1 = 2 * Dn1;
          break;
        }
        // When the prime divisor would be greater than the residual n1,
        // that residual n1 is the last prime factor with an exponent=1
        // No necessity to identify it.
        exponent = 1;
        while (n1 % CurrentPrime == 0) {
          exponent++;
          n1 = n1 / CurrentPrime;
        } // while
        if (exponent > 1) {
          Dn1 = Dn1 * exponent;
        }
        if (n1 == 1) {
          break;
        }
      } // for primes loop
      cnt = Dn * Dn1;
      Dn = Dn1;
      cout << "Num divisors=" << cnt 
        << " Tn[" << n << "]=" << (n*(n+1)/2)
        << "\tTn1[" << n1 << "]=" << (n1*(n1+1)/2)
        << "\tDn=" << Dn << " Dn1=" << Dn1 << endl;
    } // While divisors < 500
    cout << "T= " << (n * (n - 1) / 2) << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
