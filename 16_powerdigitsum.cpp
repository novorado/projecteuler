#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long long;

void doubleMe(string &s) {
  unsigned int carryOver{0};
  for (auto it = s.rbegin(); it != s.rend(); it++) {
    unsigned int val = (unsigned char)(*it) - '0';
    unsigned int next = 2 * val + carryOver;
    stringstream ss;
    ss << next;
    string num = ss.str();
    *it = *num.rbegin();
    num.pop_back();
    if (num.empty()) {
      carryOver = 0;
    } else {
      carryOver = num[0] - '0';
    }
  }
  if (carryOver) {
    char buf[2] = {(char)(carryOver + '0'), 0x0};
    s.insert(0, buf);
  }
}

size_t sum_of_digits(const string &s) {
  size_t rv{0};
  for (auto &ch : s) {
    rv += (unsigned char)(ch) - '0';
  }
  return rv;
}

int main(int, char **) {
  try {
#define N 1000
    string strNum = "1";
    for (size_t cnt = 1; cnt <= N; cnt++) {
      doubleMe(strNum);
      cout << setw(5) << cnt << " " << sum_of_digits(strNum) << endl;
    }
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
