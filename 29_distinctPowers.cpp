#include "include/primes.h"
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

using FactorizedNumber = std::map<num_t, num_t>;
using ptFactorizedNumber = shared_ptr<FactorizedNumber>;

int main(int, char **) {
  try {
#define T 100
    vector<num_t> myPrimes;
    vector<bool> f(T);
    for (int i = 1; i <= T; i++) {
      if (is_prime(i)) {
        f[i] = true;
        myPrimes.push_back(i);
      } else {
        f[i] = false;
      }
    }
    set<FactorizedNumber> x;
    size_t rv{0};
    for (num_t a = 2; a <= T; a++) {
      // record a as
      // p1^a1 * p2^a2 .. pN^aN
      FactorizedNumber fA = factorize(a, myPrimes);
      for (num_t b = 2; b <= T; b++) {
        if (f[a] && f[b]) {
          // unique
          /*
          cout << "Expecting to be unique " << a << "^" << b << " " << pow(a, b)
               << endl;
               */
          rv++;
          continue;
        }
        // Use (ab)^n = a^n * b^n
        auto fAcopy = fA;
        for (auto &p : fAcopy) {
          // Use (a^n)^m = a^(n*m)
          p.second *= b;
        }
        x.insert(fAcopy);
      }
    }
    rv += x.size();
    cout << "There are " << rv << " distinct terms" << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
