#include <iostream>
using namespace std;
int main(int, char **) {
  const int threshhold = 1000;
  int sum=0;
  for (int i = 1; i < threshhold; i++) {
      if(i%3==0) sum+=i;
      else if(i%5==0) sum+=i;
  }
  cout << "T(" << threshhold << ")=" << sum << endl;
  return 0;
}
