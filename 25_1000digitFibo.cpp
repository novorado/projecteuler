#include "include/long_numbers.h"
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;
#define N 1000
int main(int, char **) {
  try {
    list<string> fibo = {"1", "1"};
    size_t idx{2};
    for (;;) {
      idx++;
      fibo.emplace_back();
      auto it = fibo.begin();
      add(fibo.back(), *it++);
      add(fibo.back(), *it);
      //cout << idx << " " << fibo.back() << endl;
      if (fibo.back().length() >= N) {
        break;
      }
      fibo.pop_front();
    }
    cout << "Fibo with " << N << " digits " << fibo.back() << " index = " << idx
         << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
