#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
//#define N 10
#define N 2000000
using num_t = unsigned long;

bool is_prime(num_t n) {
  if (n == 1)
    return false;
  if (n < 4)
    return true;
  if (n % 2==0)
    return false;
  if (n < 9)
    return true;
  if (n % 3 == 0)
    return false;
  /*
  Any number n can have only one primefactor greater than n .
  The consequence for primality testing of a number n is: if we cannot find a
  number f less than or equal n that divides n then n is prime: the only
  primefactor of n is n itself
  All primes greater than 3 can be written in the form 6k+/-1.
  */
  num_t r = floor(sqrt(n));
  num_t f = 5;
  while (f <= r) {
    if (n % f == 0)
      return false;
    if (n % (f + 2) == 0)
      return false;
    f += 6;
  }
  return true;
}

int main(int, char **) {
  num_t pSum = 2;
  for (num_t i = 3; i < N; i += 2) {
    if (is_prime(i)) {
      pSum += i;
    }
  }
  cout << "pSum(" << N << ")= " << pSum << endl;
  return 0;
}
