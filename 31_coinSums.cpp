#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;
#define T 200

vector<num_t> coins{1, 2, 5, 10, 20, 50, 100, 200}, upperBound(coins.size());

void print_solution(const list<long> &s) {
  static int numSolution = 0;
  size_t idx = 0;
  cout << (++numSolution) << ") ";
  long checkSum=0;
   for (auto &n : s) {
    if (n)
    {
       cout << n << "*" << coins[idx] << " ";
       checkSum+=n*coins[idx];
    }
    idx++;
  }
  cout << endl;
  if(checkSum!=T)
  {
      throw logic_error("Wrong checksum");
  }
}

void iterate(long sum = T) {
  static list<long> cnt;
  auto depth = cnt.size();
  cnt.push_back(0);
  auto &i = cnt.back();
  for (i = 0; i <= upperBound[depth]; i++) {
    long d = sum - i * coins[depth];
    if (d == 0) {
      print_solution(cnt);
    } else {
      if (d < 0) {
        // No solution in this permutation
        cnt.pop_back();
        return;
      }
      // d > 0, keep exploring
      if (cnt.size() < coins.size()) {
        iterate(sum - i * coins[depth]);
      }
    }
  }
  cnt.pop_back();
}

int main(int, char **) {
  try {
    // Upper bounds
    size_t idx = 0;
    for (auto &v : upperBound) {
      v = T / coins[idx++];
    }
    iterate();
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
