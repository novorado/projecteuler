#include <iostream>
using namespace std;
int main(int, char **) {
  const int threshhold = 4000000;
  int sum=0;
  int n1=1,n2=2;
  while(n2<threshhold)
  {
      if(n2%2==0) sum+=n2;
      int next=n2+n1;
      //cout << "next=" << next << endl;
      n1=n2;
      n2=next;
  }
  cout << "T(" << threshhold << ")=" << sum << endl;
  return 0;
}
