#include "include/primes.h"
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

// function for GCD
int GCD(int a, int b) {
  if (b == 0)
    return a;
  return GCD(b, a % b);
}

// Fucnction return smallest +ve integer that
// holds condition A^k(mod N ) = 1
int multiplicativeOrder(int A, int N) {
  if (GCD(A, N) != 1)
    return -1;

  // result store power of A that rised to
  // the power N-1
  unsigned int result = 1;

  int K = 1;
  while (K < N) {
    // modular arithmetic
    result = (result * A) % N;

    // return samllest +ve integer
    if (result == 1)
      return K;

    // increment power
    K++;
  }

  return -1;
}

int main(int, char **) {
  try {
    vector<num_t> myPrimes;
#define T 1000
    generate_primes(myPrimes, T);
    size_t max_cycle_length{0};
    size_t d{0};
    for (const auto &p : myPrimes) {
      // 1) Multiples of prime divisors d that are divisible by 2 or 5 produce
      // infinite expansion with a non-recurring prefix
      // 2) Divisors d of the form 2n×5m produce a finite expansion, e.g.
      // 1/2=0.5 (non-recurring)
      if (p == 2 || p == 5) {
        continue;
      }
      if (p > T) {
        break;
      }
      // Interestingly, the cycle length of such a divisor p in the decimal
      // representation of  1/p is the multiplicative order of 10modp. That is,
      // the cycle length is the minimum k s.t. 10k=1modp.
      auto cycle_length = multiplicativeOrder(10, p);
      if (cycle_length > max_cycle_length) {
        max_cycle_length = cycle_length;
        d = p;
      }
    }
    cout << "d=" << d << " cycle length is " << max_cycle_length << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
