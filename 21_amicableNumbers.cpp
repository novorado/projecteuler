#include "include/primes.h"
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>

#define N 10000
using namespace std;

vector<num_t> myprimes;

vector<num_t> proper_divisors(num_t n) {
  auto card = factorize(n, myprimes);
  card.erase(n);
  set<num_t> tmp;
  for (auto i : card) {
    tmp.insert(i.first);
  }
  for (auto i : card) {
    for (auto j : tmp) {
      auto prod = i.first * j;
      if (prod < n && n%prod==0) {
        tmp.insert(prod);
      }
    }
  }
  tmp.insert(1);
  return {tmp.begin(),tmp.end()};
}

num_t d(num_t n)
{
    auto pd=proper_divisors(n);
    num_t rv{0};
    for(auto i: pd) rv+=i;
    return rv;
}

int main(int, char **) {
  try {
    num_t SumOfAllAmicable{0};
    generate_primes(myprimes, N);
    set<num_t> amicable_nums;
    for(int i=2;i<N;i++)
    {
        if(amicable_nums.find(i)!=amicable_nums.end())
        {
            continue;
        }
        auto test=d(i);
        if(test==i)
        {
            continue;
        }
        if(d(test)==i)
        {
            cout << i << " and " << test << endl;
            amicable_nums.insert(test);
            amicable_nums.insert(i);
            SumOfAllAmicable+=test+i;
        }
    }
    cout << "SumOfAllAmicable = " << SumOfAllAmicable << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
