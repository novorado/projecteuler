#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned int;

struct Date {
  num_t day{0}, month{0}, year{0};

  unsigned long num() const { return year * 1000 + month * 10 + day; }

  bool operator<=(const Date &ano) const { return num() <= ano.num(); }
  bool operator>=(const Date &ano) const { return num() >= ano.num(); }
  bool leapYear() const {
    // A leap year occurs on any year evenly divisible by 4, but not on a
    // century unless it is divisible by 400.
    if (year % 100 == 0) {
      return year % 400 == 0;
    }
    return year % 4 == 0;
  }

  int daysThisMonth() const {
    static array<int, 12> rv{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    if (month == 2 && leapYear())
      return rv[month-1] + 1;
    return rv[month-1];
  }

  bool lastDayOfMonth() const { return day == daysThisMonth(); }

  void addDay() {
    if (lastDayOfMonth()) {
      day = 1;
      if (month == 12) {
        month = 1;
        year++;
        return;
      }
      month++;
      if (month > 12) {
        throw runtime_error("Invalid month");
      }
    } else {
      day++;
      if(day>31)
      {
          throw runtime_error("Invalid day");
      }
    }
  }

  void addWeek() {
    for (int i = 0; i < 7; i++)
      addDay();
  }

  string str() const {
    stringstream ss;
    ss << day << "-" << month << "-" << year;
    return ss.str();
  }
};

int main(int, char **) {
  try {
    Date B{1, 1, 1901}, E{31, 12, 2000};
    Date monday{1, 1, 1900};
    Date sunday = monday;
    for (int i = 0; i < 6; i++) {
      sunday.addDay();
    }
    int numberOfSundaysOn1stOfTheMonth = 0;
    while (sunday <= E) {
      if (sunday >= B && sunday.day == 1) {
        cout << sunday.str() << endl;
        numberOfSundaysOn1stOfTheMonth++;
      }
      sunday.addWeek();
    }
    cout << "How many Sundays fell on the first of the month during the "
            "twentieth century (1 Jan 1901 to 31 Dec 2000)?\nANSWER: "
         << numberOfSundaysOn1stOfTheMonth << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
