#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

void add(string &s, const string &what) {

  if (s.empty()) {
    s = what;
    return;
  }

  while (s.length() < what.length()) {
    s.insert(0, "0");
  }

  auto sWalker = s.rbegin();
  unsigned int carryOver{0};
  // Add string
  for (int idx = what.size() - 1; idx >= 0; idx--) {
    unsigned int addValue = (unsigned char)(what[idx] - '0');
    unsigned int strValue = (unsigned char)(*sWalker - '0');
    stringstream txt;
    txt << strValue + addValue + carryOver;
    string tail = txt.str();
    // We got last character
    *sWalker = *tail.rbegin();
    sWalker++;
    // Remove stored char
    tail.pop_back();
    carryOver = tail.empty() ? 0 : stoi(tail);
  }

  // Propagate carry-over
  if (carryOver) {
    while (sWalker != s.rend()) {
      unsigned int strValue = (unsigned char)(*sWalker - '0');
      stringstream txt;
      txt << strValue + carryOver;
      string tail = txt.str();
      // We got last character
      *sWalker = *tail.rbegin();
      sWalker++;
      // Remove stored char
      tail.pop_back();
      carryOver = tail.empty() ? 0 : stoi(tail);
    }
  }
  // Looks like we gonna need extra digit
  if (carryOver) {
    stringstream txt;
    txt << carryOver;
    string tail = txt.str();
    // check
    if (tail.length() != 1) {
      throw logic_error("Unpexcted carry over string length");
    }
    s.insert(0, tail);
  }
}

void mul1(string &s, unsigned int f) {
  if (f <= 0 || f > 9) {
    throw runtime_error("mul1 takes 1 .. 9 as a second argument");
  }
  auto initial_value = s;
  while (--f) {
    add(s, initial_value);
  }
}

int sum_digits(const string& s)
{
  int rv{0};
  for(auto& ch:s)
  {
    rv+=(unsigned char)(ch)-'0';
  }
  return rv;
}

void mul(string &s, const string &arg) {
  int order{0};
  list<string> terms;
  for (auto it = arg.rbegin(); it != arg.rend(); it++, order++) {
    string term{s};
    for (int i = 0; i < order; i++) {
      term.push_back('0');
    }
    auto ch = *it - '0';
    if (!ch) {
      continue;
    }
    mul1(term, ch);
    terms.push_back(term);
  }
  // summarize terms
  s = "";
  for (auto &t : terms)
    add(s, t);
}

int main(int, char **) {
  try {
    const num_t N = 100;
    string Ni{"1"}, f{"1"};
    for (int i = 1; i < N; i++) {
      add(Ni, "1");
      mul(f, Ni);
    }
    cout << N << "! sum of all digits " << sum_digits(f) << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
