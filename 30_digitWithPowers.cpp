#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

array<unsigned long, 10> v;

unsigned long sumE(unsigned long n) {
  unsigned long rv{0};
  for (; n;) {
    unsigned long f = n - 10 * (n / 10);
    rv += v[f];
    n /= 10;
  }
  return rv;
}

unsigned long upper_bound(unsigned long e) {
  string v{"9"};
  unsigned long n{0};
  for (;;) {
    n = stoi(v);
    auto s = sumE(n);
    if (s < n) {
      break;
    }
    v += "9";
  }
  // appriximate suboptimally
  for (;;) {
    auto n1 = n / 2;
    if (sumE(n1) < n1) {
      n = n1;
    } else {
      break;
    }
  }
  return n;
}

int main(int, char **) {
  try {
#define E 5
    for (unsigned long i = 0; i <= 9; i++) {
      v[i] = pow(i, E);
    }
    size_t rv{0};
    auto ub = upper_bound(E);
    cout << "Upper bound = " << ub << endl;
    for (int i = 2; i < ub; i++) {
      if (i == sumE(i)) {
        cout << i << " ";
        rv += i;
      }
    }
    cout << ".. sum is " << rv << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
