#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <vector>
#include <iomanip>
using namespace std;
using num_t = unsigned long;

//#define N 4
#define N 20
array<num_t, (N + 1) * (N + 1)> matrix;

num_t &p(size_t x, size_t y) {
  if (x > N || y > N) {
    throw runtime_error("index out of range");
  }
  return matrix[x * (N + 1) + y];
}

void print_matrix() {
  for (int i = 0; i <= N; i++) {
    for (int j = 0; j <= N; j++) {
      cout << setw(3) << p(j, i) << " ";
    }
    cout << endl;
  }
  cout << endl;
}

int main(int, char **) {
  try {
    fill(matrix.begin(), matrix.end(), 0x0);
    p(0, 0) = 1;
    for (size_t y = 0; y <= N; y++) {
      for (size_t x = 0; x <= N; x++) {
        if (x < N) { // right
          //cout << x << "," << y << " right" << endl;
          p(x + 1, y) += p(x, y);
          //print_matrix();
        }
        if (y < N) { // down
          //cout << x << "," << y << " down" << endl;
          p(x, y + 1) += p(x, y);
          //print_matrix();
        }
      }
    }
    cout << "Total paths(" << N << ")=" << p(N, N) << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
