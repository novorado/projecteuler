#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

num_t score(const string &name) {
  num_t rv{0};
  for (auto &ch : name) {
    rv += (unsigned int)(ch - 'A' + 1);
  }
  return rv;
}

int main(int, char **) {
  try {
    vector<string> names;
    ifstream in("data/p022_names.txt");

    while (in) {
      string s;
      if (!getline(in, s))
        break;

      istringstream ss(s);

      while (ss) {
        string s;
        if (!getline(ss, s, ','))
          break;
        s.erase(remove(s.begin(), s.end(), '\"'), s.end());
        names.push_back(s);
      }
    }
    if (!in.eof()) {
      cerr << "File is not valid\n";
    }
    std::sort(names.begin(), names.end());
    num_t pos{0};
    num_t rv{0};
    for (auto &s : names) {
      int Uscore = (score(s) * ++pos);
      rv+=Uscore;
      //cout << s << "\t" << (score(s) * ++pos) << endl;
    }
    cout << "Tscore= " << rv << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
