#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = long;
using distrib = map<num_t, size_t>;

void factorize(num_t n, distrib &d) {
  auto tmp = n;
  distrib factors;
  for (num_t i = 2; i <= n; i++) {
    if (n % i == 0) {
      factors[i]++;
      n = n / i; // store factor
      i = 1;     // restart
    }
  }
  for (auto &p : factors) {
    cout << tmp << " => " << p.first << " x " << p.second << endl;
    auto it = d.find(p.first);
    if (it == d.end()) {
      d[p.first] = p.second;
    } else {
      it->second = max(it->second, p.second);
    }
  }
}

int main(int, char **) {
  //const num_t T = 10;
  const num_t T = 20;
  int res = 1;
  distrib d;

  for (num_t i = 2; i <= T; i++) {
    factorize(i, d);
  }

  for (const auto &p : d) {
    cout << "first=" << p.first << " p.second=" << p.second
         << " pow=" << pow(p.first, p.second) << endl;
    res *= pow(p.first, p.second);
  }

  cout << "Smallest number is " << res << endl;

  // check
  for (int i = 1; i <= T; i++) {
    if (res % i) {
      cout << "FAIL with " << i << endl;
    }
  }
  return 0;
}
