#include <algorithm>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned int;

#define p(x,y) case x: return #y;

string stringify(size_t n)
{
    switch(n)
    {
    p(1,one)
    p(2,two) 
    p(3,three) 
    p(4,four) 
    p(5,five) 
    p(6,six) 
    p(7,seven)
    p(8,eight)
    p(9,nine) 
    p(10,ten)
    p(11,eleven) 
    p(12,twelve)
    p(13,thirteen) 
    p(14,fourteen)
    p(15,fifteen)
    p(16,sixteen) 
    p(17,seventeen)
    p(18,eighteen)
    p(19,nineteen)
    p(20,twenty)
    p(30,thirty)
    p(40,forty)
    p(50,fifty)
    p(60,sixty)
    p(70,seventy)
    p(80,eighty)
    p(90,ninety)
    default:
    throw runtime_error("Unable to stringify value");
    break;
    }
    return "";
}

string str(size_t n)
{

    string rv;
    size_t val{0};
    if(val=n/1000)
    {
        rv += stringify(val) + " thousand";
        n-=val*1000;
    }

    if(val=n/100)
    {
        if(!rv.empty())
        {
            rv += " ";
        }
        rv += stringify(val) + " hundred";
        n -= val*100;
    }

    if(n && n<20)
    {
        if(!rv.empty())
        {
            rv += " and ";
        }
        rv += stringify(n);
        return rv;
    }

    if(val=n/10)
    {
        if(!rv.empty())
        {
            rv += " and ";
        }
        rv += stringify(val*10);
        n -= val*10;
    }

    if(n)
    {
        if(!rv.empty())
        {
            rv += " ";
        }
        rv += stringify(n);
    }

    return rv;
}

size_t length(string s)
{
    size_t rv{0};
    for(auto& ch: s) rv+= !isspace(ch)?1:0;
    return rv;
}

int main(int, char **) {
  try {
      size_t N{0};
      for(int i=1;i<=1000;i++)
      {
          string s=str(i);
          size_t lg=length(s);
          N+=lg;
          cout << setw(5) << i << " N= " << setw(5) << N << " " << s << endl;
      }
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
