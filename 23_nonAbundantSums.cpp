#include "include/primes.h"
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

std::vector<num_t> myprimes;

enum class Type { deficient, perfect, abundant };

string strType(Type t) {
#define P(x)                                                                   \
  case Type::x:                                                                \
    return #x;

  switch (t) { P(deficient) P(perfect) P(abundant) }
  throw runtime_error("Wrong number type");
  return "";
}

num_t sum_proper_divisors(num_t n) {
  auto v = proper_divisors(n, myprimes);
  num_t rv{0};
  for (const auto &n : v)
    rv += n;
  return rv;
}

Type detectNumber(num_t n) {
  auto spd = sum_proper_divisors(n);
  if (spd < n)
    return Type::deficient;
  if (spd == n)
    return Type::perfect;
  return Type::abundant;
}

int main(int, char **) {
  try {
    // integers above can be expressed with 2 abundant numbers
#define N 28123
    std::vector<bool> shadow(N);
    fill(shadow.begin(), shadow.end(), false);
    generate_primes(myprimes, N);
    vector<num_t> abundants;
    cout << "Searching for abundant numbers" << endl;
    for (num_t i = 2; i < N; i++) {
      if (detectNumber(i) == Type::abundant) {
        abundants.push_back(i);
      }
      // cout << i << "\t" << strType(detectNumber(i)) << endl;
    }
    // Cross out all integers that CAN be expressed as sum of two abundant #
    for (const auto &ab1 : abundants) {
      for (const auto &ab2 : abundants) {
        auto n = ab1 + ab2;
        if (n > N) {
          break;
        }
        shadow[n] = true;
      }
    }
    // Compute the value
    num_t cannotBeExpressedWith2Abundants{0};
    for (int i = 0; i < shadow.size(); i++) {
      if (!shadow[i]) {
        cannotBeExpressedWith2Abundants += i;
      }
    }
    cout << "cannotBeExpressedWith2Abundants= "
         << cannotBeExpressedWith2Abundants << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
