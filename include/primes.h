#pragma once
#include <algorithm>
#include <cmath>
#include <vector>
#include <map>
#include <set>
using num_t = unsigned long;

bool is_prime(num_t n) {
  if (n == 1)
    return false;
  if (n < 4)
    return true;
  if (n % 2 == 0)
    return false;
  if (n < 9)
    return true;
  if (n % 3 == 0)
    return false;
  /*
  Any number n can have only one primefactor greater than n .
  The consequence for primality testing of a number n is: if we cannot find a
  number f less than or equal n that divides n then n is prime: the only
  primefactor of n is n itself
  All primes greater than 3 can be written in the form 6k+/-1.
  */
  num_t r = std::floor(std::sqrt(n));
  num_t f = 5;
  while (f <= r) {
    if (n % f == 0)
      return false;
    if (n % (f + 2) == 0)
      return false;
    f += 6;
  }
  return true;
}

void generate_primes(std::vector<num_t> &primes, size_t sz) {
  primes.reserve(sz);
  for (num_t p = 2; primes.size() < sz; p++)
    if (is_prime(p))
      primes.push_back(p);
}

std::map<num_t, num_t> factorize(num_t n, const std::vector<num_t> &primes) {
  std::map<num_t, num_t> card;
  for (auto p : primes) {
    while (n && n % p == 0) {
      card[p]++;
      n /= p;
    }
  }
  return card;
}

std::vector<num_t> proper_divisors(num_t n, const std::vector<num_t> &myprimes) {
  auto card = factorize(n, myprimes);
  card.erase(n);
  std::set<num_t> tmp;
  for (auto i : card) {
    tmp.insert(i.first);
  }
  for (auto i : card) {
    for (auto j : tmp) {
      auto prod = i.first * j;
      if (prod < n && n % prod == 0) {
        tmp.insert(prod);
      }
    }
  }
  tmp.insert(1);
  return {tmp.begin(), tmp.end()};
}