#pragma once
#include <string>
#include <sstream>
#include <list>

void add(std::string &s, const std::string &what) {

  if (s.empty()) {
    s = what;
    return;
  }

  while (s.length() < what.length()) {
    s.insert(0, "0");
  }

  auto sWalker = s.rbegin();
  unsigned int carryOver{0};
  // Add string
  for (int idx = what.size() - 1; idx >= 0; idx--) {
    unsigned int addValue = (unsigned char)(what[idx] - '0');
    unsigned int strValue = (unsigned char)(*sWalker - '0');
    std::stringstream txt;
    txt << strValue + addValue + carryOver;
    std::string tail = txt.str();
    // We got last character
    *sWalker = *tail.rbegin();
    sWalker++;
    // Remove stored char
    tail.pop_back();
    carryOver = tail.empty() ? 0 : stoi(tail);
  }

  // Propagate carry-over
  if (carryOver) {
    while (sWalker != s.rend()) {
      unsigned int strValue = (unsigned char)(*sWalker - '0');
      std::stringstream txt;
      txt << strValue + carryOver;
      std::string tail = txt.str();
      // We got last character
      *sWalker = *tail.rbegin();
      sWalker++;
      // Remove stored char
      tail.pop_back();
      carryOver = tail.empty() ? 0 : stoi(tail);
    }
  }
  // Looks like we gonna need extra digit
  if (carryOver) {
    std::stringstream txt;
    txt << carryOver;
    std::string tail = txt.str();
    // check
    if (tail.length() != 1) {
      throw std::logic_error("Unpexcted carry over string length");
    }
    s.insert(0, tail);
  }
}

void mul1(std::string &s, unsigned int f) {
  if (f <= 0 || f > 9) {
    throw std::runtime_error("mul1 takes 1 .. 9 as a second argument");
  }
  auto initial_value = s;
  while (--f) {
    add(s, initial_value);
  }
}

int sum_digits(const std::string &s) {
  int rv{0};
  for (auto &ch : s) {
    rv += (unsigned char)(ch) - '0';
  }
  return rv;
}

void mul(std::string &s, const std::string &arg) {
  int order{0};
  std::list<std::string> terms;
  for (auto it = arg.rbegin(); it != arg.rend(); it++, order++) {
    std::string term{s};
    for (int i = 0; i < order; i++) {
      term.push_back('0');
    }
    auto ch = *it - '0';
    if (!ch) {
      continue;
    }
    mul1(term, ch);
    terms.push_back(term);
  }
  // summarize terms
  s = "";
  for (auto &t : terms)
    add(s, t);
}
