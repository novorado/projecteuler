#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

using num_t = unsigned long;
int main(int, char **) {
  const num_t upperBound = 500;
  for (num_t a = 0; a <= upperBound; a++) {
    for (num_t b = 0; b <= upperBound; b++) {
      num_t c = sqrt(a * a + b * b);
      if (a + b + c == 1000) {
        cout << "a=" << a << " b=" << b << " c=" << c << endl
      }
    }
  }
  return 0;
}
