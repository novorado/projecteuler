#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
#include <iomanip>
#include <stack>
using namespace std;
using num_t = unsigned char;

num_t data2[]={
75,
95,64,
17,47,82,
18,35,87,10,
20,04,82,47,65,
19,01,23,75,03,34,
88,02,77,73,07,63,67,
99,65,04,28,06,16,70,92,
41,41,26,56,83,40,80,70,33,
41,48,72,33,47,32,37,16,94,29,
53,71,44,65,25,43,91,52,97,51,14,
70,11,33,28,77,73,17,78,39,68,17,57,
91,71,52,38,17,14,91,43,58,50,27,29,48,
63,66,04,68,89,53,67,30,73,16,69,87,40,31,
04,62,98,27,23, 9,70,98,73,93,38,53,60,04,23
};

num_t data1[]={
3,
7, 4,
2, 4, 6,
8, 5, 9, 3
};

#define data data2

const num_t& p(size_t level, size_t n)
{
    // Level starts with 0
    level+=1;
    if(n>level)
    {
        throw logic_error("Can not exceed number of nodes per level");
    }
    auto idx = level * (level + 1) / 2 - level + n; 
    return data[idx];
}

size_t num_levels()
{
    // Num nodes N=L*(L+1)/2
    auto Nnodes = sizeof(data) / sizeof(num_t);
    // L^2 + L - 2N = 0
    // a=1, b = 1, c = -2N
    int a = 1, b = 1, c = -2 * Nnodes;

    // ax^2+bx+c=0
    // D=b^2-4ac
    int D = b*b - 4 * a * c;

    if(D<0)
    {
        throw logic_error("Array does not contain tree data");
    }

    int x1 = ( -b + sqrt(D)) / (2 * a);
    int x2 = ( -b - sqrt(D)) / (2 * a);

    return x1>0? x1 : x2;
}

struct coord
{
    size_t level{0}, pos{0};

    const int value() const { return p(level,pos); }
    coord left() const { return {level+1,pos}; }
    coord right() const { return {level+1,pos+1}; }
};

template <typename OS> OS& operator << (OS& os, const coord& c)
{
    return os << c.level << "[" << c.pos << "]=" << c.value();
}

void traverse(coord c, size_t depth, vector<coord>& trace) {
  trace.push_back(c);
  if (depth == 0) {
    trace.pop_back();
    int Weight = 0;
    for(auto& tc: trace)
    {
        Weight+=tc.value();
    }
    static int maxWeigth=0;
    if(Weight>maxWeigth)
    {
        for (auto &tc : trace)
        {
            cout << tc.value() << " ";
        }
        maxWeigth=Weight;
        cout << " WEIGHT=" << Weight << endl;
    }
    return;
  }
  // cout << setw(depth+1) << c << " Weigth=" << Weight << endl;
  traverse(c.left(), depth - 1, trace);
  traverse(c.right(), depth - 1, trace);
  trace.pop_back();
}

int main(int, char **) {
  try {
      auto Nl=num_levels();
      vector<coord> trace;
      trace.reserve(Nl);
      traverse({0,0},Nl,trace);
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
