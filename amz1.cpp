#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);

/*
 * Complete the 'getNumberOfOptions' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY priceOfJeans
 *  2. INTEGER_ARRAY priceOfShoes
 *  3. INTEGER_ARRAY priceOfSkirts
 *  4. INTEGER_ARRAY priceOfTops
 *  5. INTEGER dollars
 */

long getNumberOfOptions(vector<int> priceOfJeans, vector<int> priceOfShoes,
                        vector<int> priceOfSkirts, vector<int> priceOfTops,
                        int dollars) {
  int numVariations = 0;
  priceOfJeans.push_back(0);
  priceOfShoes.push_back(0);
  priceOfSkirts.push_back(0);
  priceOfTops.push_back(0);
  for (const auto &jeans : priceOfJeans) {
    int pocket = dollars - jeans;
    if (pocket <= 0) {
      if (!pocket)
        numVariations++;
      pocket += jeans;
      continue;
    }
    for (const auto &shoe : priceOfShoes) {
      pocket -= shoe;
      if (pocket <= 0) {
        if (!pocket)
          numVariations++;
        pocket += shoe;
        continue;
      }
      for (const auto &skirt : priceOfSkirts) {
        pocket -= skirt;
        if (pocket <= 0) {
          if (!pocket)
            numVariations++;
          pocket += skirt;
          continue;
        }
        for (const auto &top : priceOfTops) {
          pocket -= top;
          if (pocket <= 0) {
            if (!pocket)
              numVariations++;
            pocket += top;
            continue;
          }
          pocket+=top;
        }//top
        pocket+=skirt;
      }//skirt
      pocket+=shoe;
    }//shoe
    pocket+=jeans;
  }//jeans
  return numVariations;
}

int main() {
  ofstream fout(getenv("OUTPUT_PATH"));

  string priceOfJeans_count_temp;
  getline(cin, priceOfJeans_count_temp);

  int priceOfJeans_count = stoi(ltrim(rtrim(priceOfJeans_count_temp)));

  vector<int> priceOfJeans(priceOfJeans_count);

  for (int i = 0; i < priceOfJeans_count; i++) {
    string priceOfJeans_item_temp;
    getline(cin, priceOfJeans_item_temp);

    int priceOfJeans_item = stoi(ltrim(rtrim(priceOfJeans_item_temp)));

    priceOfJeans[i] = priceOfJeans_item;
  }

  string priceOfShoes_count_temp;
  getline(cin, priceOfShoes_count_temp);

  int priceOfShoes_count = stoi(ltrim(rtrim(priceOfShoes_count_temp)));

  vector<int> priceOfShoes(priceOfShoes_count);

  for (int i = 0; i < priceOfShoes_count; i++) {
    string priceOfShoes_item_temp;
    getline(cin, priceOfShoes_item_temp);

    int priceOfShoes_item = stoi(ltrim(rtrim(priceOfShoes_item_temp)));

    priceOfShoes[i] = priceOfShoes_item;
  }

  string priceOfSkirts_count_temp;
  getline(cin, priceOfSkirts_count_temp);

  int priceOfSkirts_count = stoi(ltrim(rtrim(priceOfSkirts_count_temp)));

  vector<int> priceOfSkirts(priceOfSkirts_count);

  for (int i = 0; i < priceOfSkirts_count; i++) {
    string priceOfSkirts_item_temp;
    getline(cin, priceOfSkirts_item_temp);

    int priceOfSkirts_item = stoi(ltrim(rtrim(priceOfSkirts_item_temp)));

    priceOfSkirts[i] = priceOfSkirts_item;
  }

  string priceOfTops_count_temp;
  getline(cin, priceOfTops_count_temp);

  int priceOfTops_count = stoi(ltrim(rtrim(priceOfTops_count_temp)));

  vector<int> priceOfTops(priceOfTops_count);

  for (int i = 0; i < priceOfTops_count; i++) {
    string priceOfTops_item_temp;
    getline(cin, priceOfTops_item_temp);

    int priceOfTops_item = stoi(ltrim(rtrim(priceOfTops_item_temp)));

    priceOfTops[i] = priceOfTops_item;
  }

  string dollars_temp;
  getline(cin, dollars_temp);

  int dollars = stoi(ltrim(rtrim(dollars_temp)));

  long result = getNumberOfOptions(priceOfJeans, priceOfShoes, priceOfSkirts,
                                   priceOfTops, dollars);

  fout << result << "\n";

  fout.close();

  return 0;
}

string ltrim(const string &str) {
  string s(str);

  s.erase(s.begin(),
          find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));

  return s;
}

string rtrim(const string &str) {
  string s(str);

  s.erase(
      find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
      s.end());

  return s;
}
