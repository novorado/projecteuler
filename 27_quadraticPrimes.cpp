#include "include/primes.h"
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

#define PRIME_TABLE_SZ 500000
vector<bool> primesVector(PRIME_TABLE_SZ);

size_t numPrimes(long a, long b) {
  size_t rv{0};
  for (;; rv++) {
    long idx = rv * rv + a * rv + b;
    if (idx < 0) {
      break;
    }
    if(idx>=primesVector.size())
    {
        throw logic_error("idx is too big");
    }
    if (!primesVector[idx]) {
      break;
    }
  }
  return rv;
}

int main(int, char **) {
  try {
#define T 1000
    {
      primesVector.resize(PRIME_TABLE_SZ);
      for (int i = 0; i < PRIME_TABLE_SZ; i++) {
        primesVector[i] = is_prime(i);
      }
    }
    struct {
      size_t res{0};
      int a{0}, b{0};
    } maxRes;
    for (int a = -T + 1; a < T; a++) {
      for (int b = -T; b <= T; b++) {
        auto res = numPrimes(a, b);
        if (res > maxRes.res) {
          maxRes.res = res;
          maxRes.a = a;
          maxRes.b = b;
        }
      }
    }
    cout << "n=" << maxRes.res << " a=" << maxRes.a << " b=" << maxRes.b
         << " answer: " << (maxRes.a * maxRes.b) << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
