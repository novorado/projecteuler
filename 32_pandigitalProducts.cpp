#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

#define UPPER_BOUND 987654321
vector<bool> marks(987654321);

void mark(list<unsigned char> x = {}) {
  // mark out this permutation
  if (x.size() >= 1) {
    unsigned long num{0};
    for (const auto &ch : x) {
      if (num)
        num *= 10;
      num += ch;
    }
    marks[num] = true;
  }
  set<unsigned char> s{x.begin(), x.end()};
  // find next permutation
  x.emplace_back();
  auto &i = x.back();
  for (i = 1; i <= 9; i++) {
    if (s.find(i) == s.end()) {
      mark(x);
    }
  }
}

bool insert(long n, unsigned int &x) {
  while (n) {
    long f = n - 10 * (n / 10) - 1;
    unsigned int mask = (1 << f);
    if (x & mask) {
      return false;
    }
    x |= mask;
    n /= 10;
  }
  return true;
}

bool is_unusual(long i, long j, long mul) {
  unsigned int x{0x0};
  if (insert(i, x) && insert(j, x) && insert(mul, x)) {
    return x == 0x1ff;
  }
  return false;
}

long upper_bound(long n)
{
    unsigned int x{0};
    insert(n,x);
    unsigned int rv{0x0};
    unsigned int mask=1;
    for(int i=1;i<=9;i++)
    {
        if( ! (n & mask))
        {
            rv|=mask;
        }
        mask<<=1;
    }
    return rv;
}

int main(int, char **) {
  try {
    set<long> counted;
    long totalSum = 0;
    fill(marks.begin(), marks.end(), false);
    mark();
    for (long i = 9876; i>=1; i--) {
      if (marks[i]) {
        auto ub = upper_bound(i);
        for (long j = 1; j < ub; j++) {
          if (marks[j]) {
            long mul = i * j;
            if (mul > UPPER_BOUND) {
              throw logic_error("overflow");
            }
            if (marks[mul]) {
              if (is_unusual(i, j, mul)) {
                static set<long> counted;
                if (counted.find(mul) != counted.end()) {
                  continue;
                }
                counted.insert(mul);
                totalSum += mul;
                cout << i << "*" << j << " = " << (mul) << endl;
                break; // increment i
              }
            }
          }
        }
      }
    }
    cout << "Sum is " << totalSum << endl;
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
