#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;
#define N 100

int main(int, char **) {
    num_t sum=0, sum_sq=0;
    for(num_t i=1;i<=N;i++)
    {
        sum+=i;
        sum_sq+=i*i;
    }
    num_t diff=sum*sum - sum_sq;
    cout << "N=" << N << " diff= " << diff << endl;
  return 0;
}
