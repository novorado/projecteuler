#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
using num_t = unsigned long;

#define N 9 // WARNING! 9 is max num, wont work otherwise
using mySet = set<unsigned char>;

void permutate(const mySet &s) {
  static array<unsigned char, N + 1> v;
  size_t order = v.size() - s.size();
  for (const auto &ch : s) {
    v[order] = ch;
    auto nextSet = s;
    nextSet.erase(ch);
    if (!nextSet.empty()) {
      permutate(nextSet);
    } else {
      static size_t iter{0};
      if (++iter == 1000000) {
        for (const auto &ch : v) {
          cout << (unsigned int)(ch);
        }
        cout << endl;
        exit(0);
      }
    }
  }
}

int main(int, char **) {
  try {
    mySet s;
    for (int i = 0; i <= N; i++)
      s.insert(i);
    permutate(s);
  } catch (const exception &e) {
    cout << "Error: " << e.what() << endl;
  }
  return 0;
}
